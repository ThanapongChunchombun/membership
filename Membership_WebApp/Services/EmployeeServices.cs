﻿using Membership_WebApp.Models;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using System.Text;
using System.Text.Json;

namespace Membership_WebApp.Services
{
    public interface IEmployeeServices
    {
        Task<List<Employee>> GetEmployees();
        Task<Employee> GetEmployeeById(string id);
        Task InsertEmployee(Employee model);

        Task DeleteEmployeeById(string Id);
        Task OnUpdateEmployeeById(string Id, Employee updateEmployee);

    }
    public class EmployeeServices : IEmployeeServices

    //public class EmployeeServices
    {
        private HttpClient _httpClient;
        private IHttpService _httpService;
        private NavigationManager _navigationManager;
        private ILocalStorageService _localStorageService;
        public User User { get; private set; }

        public EmployeeServices(
            IHttpService httpService, 
            HttpClient httpClient, 
            NavigationManager navigationManager,
            ILocalStorageService localStorageService
            )
        {
            _httpService = httpService;
            _httpClient = httpClient;
            _navigationManager = navigationManager;
            _localStorageService = localStorageService;
        }

        //public async Task<IEnumerable<Employee>> GetEmployees()
        public async Task<List<Employee>> GetEmployees()
        {
            var token = await _localStorageService.GetItem<string>("AccessToken");
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, "api/Account");

            requestMessage.Headers.Authorization
                = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonSerializer.Deserialize<List<Employee>>(responseBody));
            }
            else
                return null;


            //return null;
            //return await _httpClient.GetFromJsonAsync<Employee[]>("api/Account");
            //return null;
        }

        public async Task<Employee> GetEmployeeById(string id)
        {
            //return null;
            //return await _httpClient.GetFromJsonAsync<Employee>($"api/Account/{id}");
            var token = await _localStorageService.GetItem<string>("AccessToken");
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"api/Account/{id}");

            requestMessage.Headers.Authorization
                = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonSerializer.Deserialize<Employee>(responseBody));
            }
            else
                return null;
        }

        public async Task InsertEmployee(Employee model)
        {
            //var res = await _httpClient.PostAsJsonAsync("api/Account", model);
            //if (res.IsSuccessStatusCode)
            //{
            //    _navigationManager.NavigateTo("/employee");
            //}
            string serializedUser = JsonSerializer.Serialize(model);
            var token = await _localStorageService.GetItem<string>("AccessToken");
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "api/Account");

            requestMessage.Headers.Authorization
                = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");


            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            //Console.WriteLine(responseStatusCode.ToString());
            if ((responseStatusCode.ToString()) == "Created")
            {
                _navigationManager.NavigateTo("/employee");
                //var responseBody = await response.Content.ReadAsStringAsync();
                //return await Task.FromResult(JsonSerializer.Deserialize<Employee>(responseBody));
            }
        }

        public async Task DeleteEmployeeById(string Id)
        {
            var token = await _localStorageService.GetItem<string>("AccessToken");

            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, $"api/Account/{Id}");

            requestMessage.Headers.Authorization
                = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            //Console.WriteLine(responseStatusCode.ToString());
            if ((responseStatusCode.ToString()) == "NoContent")
            {
                _navigationManager.NavigateTo("/employee");
            }
            //return null;
            //var res = await _httpClient.DeleteAsync($"api/Account/{Id}");
            //if (res.IsSuccessStatusCode)
            //{
            //    //var Id = await res.Content.ReadAsStringAsync();
            //    _navigationManager.NavigateTo("/employee");
            //}
        }
        public async Task OnUpdateEmployeeById(string Id, Employee updateEmployee)
        {
            var token = await _localStorageService.GetItem<string>("AccessToken");
            string serializedUser = JsonSerializer.Serialize(updateEmployee);

            var requestMessage = new HttpRequestMessage(HttpMethod.Put, $"api/Account/{Id}");

            requestMessage.Headers.Authorization
                = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            if ((responseStatusCode.ToString()) == "NoContent")
            {
                _navigationManager.NavigateTo("/employee");
            }
            //return null;
            //var res = await _httpClient.PutAsJsonAsync($"api/Account/{Id}", updateEmployee);
            //if (res.IsSuccessStatusCode)
            //{
            //    //var Id = await res.Content.ReadAsStringAsync();
            //    _navigationManager.NavigateTo("/employee");
            //}
        }


    }
}
