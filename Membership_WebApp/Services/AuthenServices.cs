﻿using Membership_API.Helper;
using Membership_WebApp.Helpers;
using Membership_WebApp.Models;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson.IO;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace Membership_WebApp.Services
{
    public interface IAuthenServices
    {
        RegisterToken_Model User { get; }
        Task Initialize();
        Task Login(Login_Model model);
        Task Logout();
        Task Register(Register_Model user);
        //Task<RegisterToken_Model> RegisterGetRefreshToken(Register_Model user);
        Task<string> RegisterGetRefresh(Register_Model user);
        Task<RegisterToken_Model> LoginGetToken(Login_Model user);

    }
    public class AuthenServices : IAuthenServices
    {
        private HttpClient _httpClient;
        private NavigationManager _navigationManager;
        private ILocalStorageService _localStorageService;
        private string _userKey = "user";
        public RegisterToken_Model User { get; private set; }

        public AuthenServices(
            HttpClient httpClient,
            NavigationManager navigationManager,
            ILocalStorageService localStorageService

        )
        {
            _httpClient = httpClient;
            _navigationManager = navigationManager;
            _localStorageService = localStorageService;
        }

        public async Task Initialize()
        {
            User = await _localStorageService.GetItem<RegisterToken_Model>(_userKey);
        }

        public async Task Login(Login_Model model)
        {
            var res = await _httpClient.PostAsJsonAsync("api/Authen", model);
            if (res.IsSuccessStatusCode)
            {
                var resUser = await res.Content.ReadAsStringAsync();
                var body = JsonSerializer.Deserialize<User>(resUser);
                await _localStorageService.SetItem(_userKey, body);
                _navigationManager.NavigateTo("/");
            }
            //await _httpService.Post("api/Account", model);
        }
        public async Task Logout()
        {
            User = null;
            await _localStorageService.RemoveItem(_userKey);
            await _localStorageService.RemoveItem("AccessToken");
            _navigationManager.NavigateTo("/login");
        }
        public async Task Register(Register_Model user)
        {
            var res = await _httpClient.PostAsJsonAsync("api/Register", user);
            if (res.IsSuccessStatusCode)
            {
                _navigationManager.NavigateTo("/login");
            }

        }

        public async Task<string> RegisterGetRefresh(Register_Model user)
        {
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(user.Password);
            if(passwordHash != null)
            {
                user.Password = passwordHash;
                var res = await _httpClient.PostAsJsonAsync("api/Authen/RegisterToken", user);

                if (res.IsSuccessStatusCode)
                {
                    var responseBody = await res.Content.ReadAsStringAsync();
                    return responseBody;
                }
 
            }
            return null;

        }
        public async Task<RegisterToken_Model> LoginGetToken(Login_Model user)
        {

            var res = await _httpClient.PostAsJsonAsync("api/Authen/AuthenticationToken", user);
            if (res.IsSuccessStatusCode)
            {
                var resUser = await res.Content.ReadAsStringAsync();
                var body = JsonSerializer.Deserialize<RegisterToken_Model>(resUser);

                await _localStorageService.SetItem(_userKey, body);
                await _localStorageService.SetItem("AccessToken", (body.AccessToken));
                return await Task.FromResult(body);
            }
            else { return null; }

        }


    }
}
