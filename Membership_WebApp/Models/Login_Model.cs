﻿using System.ComponentModel.DataAnnotations;

namespace Membership_WebApp.Models
{
    public class Login_Model
    {
        [Required]
        public string Username { get; set; } = null!;

        [Required]
        public string Password { get; set; } = null!;
    }
}
