﻿namespace Membership_WebApp.Models
{
    public class User
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
