﻿using Membership_API.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Membership_WebApp.Models
{
    public partial class RegisterToken_Model
    {
        public string? Id { get; set; }
        public string Name { get; set; } = null!;
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }


    }
}
