﻿using Membership_WebApp.Models;

namespace Membership_API.Models
{
    public class RefreshToken
    {
        public int TokenId { get; set; }
        public int UserId { get; set; }
        public string Token { get; set; }
        public DateTime ExpiryDate { get; set; }
        public virtual Register_Model User { get; set; }
    }
}
