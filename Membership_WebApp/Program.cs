using Membership_WebApp;
using Membership_WebApp.Helpers;
using Membership_WebApp.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

//builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("https://localhost:7079/") });

//builder.Services.AddSingleton<EmployeeServices>();
builder.Services
    .AddScoped<IEmployeeServices, EmployeeServices>()
    .AddScoped<ILocalStorageService, LocalStorageService>()
    .AddScoped<IAuthenServices, AuthenServices>()
    .AddScoped<IHttpService, HttpService>();

builder.Services.AddTransient<ValidateHeaderHandler>();

var host = builder.Build();

var AuthenServices = host.Services.GetRequiredService<IAuthenServices>();
await AuthenServices.Initialize();

//await builder.Build().RunAsync();

await host.RunAsync();
